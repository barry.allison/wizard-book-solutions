#lang racket

(require "lib/stream.rkt")

;; Exercise 3.51
;;
(define (peek x)
  (newline)
  (display x))

(define x (stream-map peek (stream-enumerate-interval 0 10)))
; 0
(stream-ref x 5)
; 1 2 3 4 5
(stream-ref x 7)
; 6 7
