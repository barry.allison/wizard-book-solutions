#lang racket

;; Ex 3.17
;;
(require scheme/mpair)

(define count-pairs
  (let ((seen null))
    (lambda (x)
      (cond ((not (mpair? x)) 0)
            ((mmemq (mcar x) seen)  0)
            (else (set! seen (mcons (mcar x ) seen))
                  (+ (count-pairs (mcar x))
                     (count-pairs (mcdr x))
                     1))))))

;(define l (mlist 'a 'b 'c))
;(set-mcdr! (mcdr (mcdr l)) l)
;(count-pairs l)
;; => 3