#lang racket

;; Ex 3.25
;;
(require scheme/mpair)

(define (mcaar xs) (mcar (mcar xs)))
(define (mcadr xs) (mcar (mcdr xs)))
(define (mcdar xs) (mcdr (mcar xs)))
(define (mcddr xs) (mcdr (mcdr xs))) 

(define (make-table)
  (define local-table (mlist '*table*))

  (define (assoc key records)
    (cond ((null? records) #f)
          ((equal? key (mcaar records)) (mcar records))
          (else (assoc key (mcdr records)))))
  
  (define (lookup keys)
    (let ((record (assoc keys (mcdr local-table))))
      (if record 
          (mcdr record)
          #f)))
  
  (define (insert! keys value)
    (let ((record (assoc keys (mcdr local-table))))
      (if record 
          (set-mcdr! record value)
          (set-mcdr! local-table (mcons (mcons keys value) (mcdr local-table))))))
  
  (define (dispatch m)
    (cond ((eq? m 'lookup) lookup)
          ((eq? m 'insert!) insert!)
          (else (error "Unknown operation -- TABLE" m))))
  dispatch)

;(define t (make-table))
;((t 'insert!) (mlist 'math '*) "mul")
;((t 'insert!) (mlist 'math '+) "plus")
;((t 'lookup) (mlist 'math '*))
;((t 'insert!) (mlist 'math '*) "multiply")
;((t 'lookup) (mlist 'math '*))
;
;(define t2 (make-table))
;((t2 'insert!) (mlist 'a) "a")
;((t2 'insert!) (mlist 'b) "b")
;((t2 'insert!) (mlist 'a 'a) "aa")
;((t2 'insert!) (mlist 'a 'b) "ab")
;((t2 'insert!) (mlist 'a 'a 'a) "aaa")
;((t2 'insert!) (mlist 'a 'a 'b) "aab")
;((t2 'insert!) (mlist 'a 'a 'a 'a 'a) "aaaaa")
;((t2 'lookup) (mlist 'a 'a 'a))
;((t2 'lookup) (mlist 'a 'a 'a 'a 'a))
