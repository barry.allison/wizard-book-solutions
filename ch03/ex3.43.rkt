#lang racket

;; Ex 3.43
;;

;;  Although withdrawal and deposit are serialised on single resources (accounts), 
;;  they are not serialised across resources and so exchange suffers from the same 
;;  concurrency problems as unserialised access to a single resource.
;;  
;;  Since withdraw and deposit are both serialised once the difference, d1 has been calculated 
;;  on accounts a1 and a2, d1 is added to a2 and subtracted from a1. Suppose that another 
;;  concurrent exchange occurs between account a3 and a2, with difference d2, 
;;  but that the concurrency mechanism, while allowing serialised access to a single account, 
;;  doesn’t apply correctly to two accounts and we see unexpected results.
;;  
;;  
;;  Total before exchanges: a1 + a2 + a3
;;  step 1: P1: a1 = a1 – d1
;;  step 2: P2: a2 = a2 – d2
;;  step  3: P1: a2 = a2 + d1 = a2 – d2 + d1
;;  step 4: P2: a3 = a3 + d2
;;  Total after exchanges: a1-d1 + a2-d2+d1 + a3+d2 = a1 + a2 + a3
;;
;;  It’s clear that two processes each accessing two resources and mutating both
;;  (even when serialising access to each one) suffers the same problem as 
;;  two processes accessing a single resource and mutating them.



