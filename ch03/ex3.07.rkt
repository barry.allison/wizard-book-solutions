#lang racket

(define (make-account balance password)
  (define bad-passwords 0)
  (define (call-the-cops v)
    'NEE-NAH-NEE-NAH)
  (define (withdraw amount)
    (if (>= balance amount)
        (begin (set! balance (- balance amount))
               balance)
        "Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (define (dispatch key m)
    (cond ((not (eq? key password)) 
           (set! bad-passwords (add1 bad-passwords))
           (if (> bad-passwords 7)
               call-the-cops
               (lambda (v) "Incorrect password -- MAKE-ACCOUNT")))
          ((eq? m 'withdraw) 
           (set! bad-passwords 0) 
           withdraw) 
          ((eq? m 'deposit) 
           (set! bad-passwords 0)
           deposit)
          (else (error "Unknown request -- MAKE-ACCOUNT" m))))
  dispatch)

(define (make-joint acc password joint-password)
  (define (dispatch key m)
    (cond ((not (eq? key joint-password))
           (error "Incorrect password -- MAKE-JOINT"))
          ((eq? m 'withdraw) (acc password 'withdraw))
          ((eq? m 'deposit)  (acc password 'deposit))
          (else (error "Unknown request -- MAKE-JOINT" m))))
  dispatch)
 
(define peter-acc (make-account 100 'open-sesame))
(define paul-acc (make-joint peter-acc 'open-sesame 'rosebud))
 
((peter-acc 'open-sesame 'withdraw) 10)
;; -> 90
 
((paul-acc 'rosebud 'withdraw) 10)
;; -> 80
((paul-acc 'open-sasame 'withdraw) 10)
;; -> ERROR: Incorrect password -- MAKE-JOINT