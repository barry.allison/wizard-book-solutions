#lang racket/base

(require rackunit
         "ex4.20a.rkt")

; Test suite
(define Y-combinator-tests
  (test-suite
   "Tests for the meta-circular evaluator - The Y Combinator"
   (test-case
    "Y combinator factorial"
    (test-equal? "ex4.21a: Y-Combinator factorial 10"
                 (interpret '((lambda (n)
                                ((lambda (fact)
                                   (fact fact n))
                                 (lambda (ft k)
                                   (if (= k 1)
                                       1
                                       (* k (ft ft (- k 1)))))))
                              10))
                 3628800)
    (test-equal? "ex4.21a: Y-Combinator factorial 15"
                 (interpret '((lambda (n)
                                ((lambda (fact)
                                   (fact fact n))
                                 (lambda (ft k)
                                   (if (= k 1)
                                       1
                                       (* k (ft ft (- k 1)))))))
                              15))
                 1307674368000))
   (test-case
    "Y combinator fibonacci"
    (test-equal? "ex4.21b: Y-Combinator fibonacci 15"
                 (interpret '((lambda (n)
                                ((lambda (fib)
                                   (fib fib n))
                                 (lambda (fn k)
                                   (cond ((= k 0) 0)
                                         ((= k 1) 1)
                                         (else (+ (fn fn (- k 1))
                                                  (fn fn (- k 2))))))))
                              15))
                 610))

   (test-case
    "Y combinator even?"
    (interpret '(define (f x)
                  ((lambda (x)
                     ((lambda (even? odd?)
                        (even? even? odd? x))
                      (lambda (ev? od? n)
                        (if (= n 0) true (od? ev? od? (- n 1))))
                      (lambda (ev? od? n)
                        (if (= n 0) false (ev? ev? od? (- n 1))))))
                   x)))
    (test-false "ex4.21b: Y-Combinator even - odd"
                (interpret '(f 17))))
   ))

(require rackunit/text-ui)
(run-tests Y-combinator-tests 'normal)
