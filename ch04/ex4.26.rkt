#lang racket 

(require (except-in "lib/base-eval.rkt"
                    lookup-variable-value)
         "ex4.04.rkt"  ;; boolean expression
         "ex4.05.rkt"  ;; cond with =>
         "ex4.06.rkt"  ;; let
         "ex4.07.rkt"  ;; let*
         "ex4.08.rkt"  ;; named-let
         "ex4.09.rkt"  ;; while
         "ex4.13.rkt"  ;; unbinding
         "ex4.16a.rkt"  ;; internal binding
         "ex4.20a.rkt"  ;; letrec
         ) 

(provide interpret)

;; Exercise 4.26
;;
;; this version of the evaluator includes these expression types:
;;  self-evaluating 
;;  variable
;;  quote 
;;  define =>> fun
;;  set! 
;;  if
;;  lambda 
;;  begin 
;;  application? 
;;  and =>> &&
;;  or =>> ||
;;  cond 
;;  special cond <test> => <recipient>
;;  let
;;  let*
;;  named let
;;  while
;;  letrec
;;  unless

(define (eval-unless exp env)
  (eval (make-if (unless-conditional exp)
                 (unless-exceptional exp)
                 (unless-usual exp))
        env))
 
(define (unless-conditional exp) (mcadr exp))
(define (unless-usual exp) (mcaddr exp))
(define (unless-exceptional exp)
  (if (not (null? (mcdddr exp)))
      (mcadddr exp)
      'false))

(define (install-unless-syntax)
  (put-syntax! 'unless eval-unless)
  (void))

(install-unless-syntax)

