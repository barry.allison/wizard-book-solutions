#lang racket

;; When we investigated streams we saw the constructor, cons-stream only evaluates the first item, not the second.
;; That led to the surprising (at the time) infinite list of 1s (define ones (cons-stream 1 ones).
;; To see why this is infinite look at the box and pointer diagrams.

;;  ------        -------
;; | ones |----->|   |   |....
;;  ------        -------    :
;;    ^            |         :
;;    :            v         :
;;    :           -------    :
;;    :          | 1 |   |   :
;;    :           -------    :
;;    :                      :
;;    ........................


;; The dotted lines show how the value of ones will be evaluated once forced.
;; The exact same pattern and result is in the implementation of add-assertion! in this question.


(define (add-assertion! assertion)
  (store-assertion-in-index assertion)
  (set! THE-ASSERTIONS
        (cons-stream assertion THE-ASSERTIONS))
  'ok)


;; THE-ASSERTIONS becomes an infinite, self-referential stream.
;; Using let in add-assertion! and add-rules! forces their delayed values and avoids the infinite stream.
;; I’m glad we weren’t asked to implement these primitives.
;; I’m not sure I would have spotted that problem and it could be a headache to find and fix.
