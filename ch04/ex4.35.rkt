#lang racket
(require "lib/amb.rkt")

;; Exercise 4.35

;; require is a racket keyword, but lets redefine it to match the book...  what could go wronng?
(define (a-pythagorean-triple-between low high)
  (let* ((i (an-integer-between low high))
         (j (an-integer-between i high))
         (k (an-integer-between j high)))
    (require (= (+ (square i) (square j))
                (square k)))
    (list i j k)))

