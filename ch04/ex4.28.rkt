#lang racket 

;; Exercise 4.28

;  Here is some code that would cause the lazy evaluator to generate an error 
;  if the operand is sent to eval rather than actual-value.
;
;  (interpret '(define (square x) (* x x)))
;  (interpret '(define (halve x) (floor (/ x 2))))
;  (interpret '(define (combine f g)
;                (lambda (n)
;                  (f (g n)))))
;  (interpret '((combine halve square) 10))
;
;  The reason is that the application (combine halve square) has an operator 
;  combine and two operands halve and square. 
;  If the lazy evaluator sent the operand to eval rather than actual-value they would both still
;  be thunks causing an error

;  In eval if the cond clause application? sends (eval (operator exp) env) to apply there is an error because 
;  there is no halve or square value - only the thunks containing their definition

;; ERROR! - Unknown procedure type -- APPLY (thunk halve #0=(((combine halve square false true ..