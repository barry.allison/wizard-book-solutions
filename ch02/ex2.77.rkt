#lang sicp

;
;
;
;   ;;    ;;  ;;;;;;
;   ;;    ;;  ;;    ;
;   ;;;   ;;  ;;    ;;
;   ;;;   ;;  ;;    ;;
;   ;;;;  ;;  ;;    ;
;   ;; ;  ;;  ;;;;;;
;   ;; ;; ;;  ;;   ;;
;   ;;  ; ;;  ;;    ;;
;   ;;  ;;;;  ;;    ;;
;   ;;   ;;;  ;;    ;;
;   ;;   ;;;  ;;   ;;
;   ;;    ;;  ;;;;;;
;
;
;   =======================================
;   Mutable cons cells - set-car!, set-cdr!
;   =======================================
;

;;
;; Since racket changed cons cells and lists to be immutable -
;; it's a real pain to use mutable listsm, which these exercises use
;; a lot in the table manipulation.
;; For the remaining exercises I'll use the sicp language
;; whcih includes all of the old set-car!, set-cdr! and mutable cons cells by default

;; As an exercise it may be possible to abstract away the mutation
;; with purely functional tables etc.


;; Ex 2.77
;;
; The ony procedures that can be used with a particular type are those
; defined explicitly in the interface section of the package i.e. those
; procedure installed using (put ... 'complex ...) which originally are:
;  add                 - type '(complex complex)
;  sub                 - type '(complex complex)
;  mul                 - type '(complex complex)
;  div                 - type '(complex complex)
;  make-from-real-imag - type complex
;  make-from-mag-ang   - type complex
;
; Adding these lines make those procedures part of the complex package
; interface.
;   (put 'real-part '(complex) real-part)
;   (put 'imag-part '(complex) imag-part)
;   (put 'magnitude '(complex) magnitude)
;   (put 'angle '(complex) angle)
;
; N.B. real-part, imag-part, magnitude and angle are all the TOP level generic procedures
; created in the same way as add sub mul and div and so calling
; (magnitude z) when z is a complex number defers to calling
; (magnitude z') where z' is either a polar or rectangular number
;
; (define z-rect (make-complex-from-real-imag 3 4))
; => (complex rectangular 3 . 4)
;
; 1.   (magnitude '(complex rectangular 3 . 4))
; 2.   --> (apply-generic 'magnitude '(complex rectangular 3 . 4))
; 3.*  --> (apply (get 'magnitude '(complex)) (contents '(complex rectangular 3 . 4)))
; 4.   --> (apply magnitude '(rectangular 3 . 4))
; 5.   --> (magnitude '(rectangular 3 . 4))
; 6.   --> (apply-generic 'magnitude '(rectangular 3 . 4))
; 7.** --> (apply (get 'magnitude '(rectangular)) (contents '(rectangular 3 . 4)))
; 8.   --> (apply (lambda (z)
;                   (sqrt (+ (square (real-part z))
;                            (square (imag-part z))))) '(3 . 4))
; 9.   --> (apply (lambda (z)
;                   (sqrt (+ (square (car z))
;                            (square (cdr z))))) '(3 . 4))
;10.   => 5
;
; *  (get 'magnitude '(complex))     => returns the TOP-LEVEL (generic) magnitude procedure due to scope rules
; ** (get 'magnitude '(rectangular)) => returns the magnitude procedure LOCAL to the rectangular package

;; ======================================================================
;;
;; The complex number package
;;
;; ======================================================================
(define (install-complex-package)
;  ;; imported procedures from rectangular and polar packages
  (define (make-from-real-imag x y) ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a) ((get 'make-from-mag-ang 'polar) r a))
  ;; internal procedures
  (define (add-complex z1 z2) (make-from-real-imag (+ (real-part z1) (real-part z2))
                                                   (+ (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2) (make-from-real-imag (- (real-part z1) (real-part z2))
                                                   (- (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2) (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                                                 (+ (angle z1) (angle z2))))
  (define (div-complex z1 z2) (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                                                 (- (angle z1) (angle z2))))
  ;; interface to rest of the system
  (define (tag z) (attach-tag 'complex z))
  (put 'add '(complex complex) (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex) (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex) (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex) (lambda (z1 z2) (tag (div-complex z1 z2))))
  (put 'make-from-real-imag 'complex (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'complex   (lambda (r a) (tag (make-from-mag-ang r a))))
  (put 'real-part '(complex) real-part)
  (put 'imag-part '(complex) imag-part)
  (put 'magnitude '(complex) magnitude)
  (put 'angle     '(complex) angle)
  'done)




;; ======================================================================
;;
;; To test the exercises I need an implementation of put and get.
;; These are taken directly from section 3.3.3 of the book
;; http://mitpress.mit.edu/sicp/full-text/book/book-Z-H-22.html#%_sec_3.3.3
;;
;; ======================================================================
(define (make-table)
  (let ((local-table (list '*table*)))
    (define (lookup key-1 key-2)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (cdr record)
                  false))
            false)))
    (define (insert! key-1 key-2 value)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (set-cdr! record value)
                  (set-cdr! subtable
                            (cons (cons key-2 value)
                                  (cdr subtable)))))
            (set-cdr! local-table
                      (cons (list key-1
                                  (cons key-2 value))
                            (cdr local-table)))))
      'ok)
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            ((eq? m 'table) local-table)
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

(define operation-table (make-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))

(define (square x) (* x x))

(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum -- TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum -- CONTENTS" datum)))

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (error
           "No method for these types -- APPLY-GENERIC"
           (list op type-tags))))))


;; ======================================================================
;;
;; The scheme number package
;;
;; ======================================================================
(define (install-scheme-number-package)
  (define (tag x)
    (attach-tag 'scheme-number x))
  (put 'add '(scheme-number scheme-number) (lambda (x y) (tag (+ x y))))
  (put 'sub '(scheme-number scheme-number) (lambda (x y) (tag (- x y))))
  (put 'mul '(scheme-number scheme-number) (lambda (x y) (tag (* x y))))
  (put 'div '(scheme-number scheme-number) (lambda (x y) (tag (/ x y))))
  (put 'make 'scheme-number (lambda (x) (tag x)))
  'done)


;; ======================================================================
;;
;; The rectangular number package
;;
;; ======================================================================
(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car z))
  (define (imag-part z) (cdr z))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (sqrt (+ (square (real-part z))
             (square (imag-part z)))))
  (define (angle z)
    (atan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a)
    (cons (* r (cos a)) (* r (sin a))))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangular x))
  (put 'real-part '(rectangular) real-part)
  (put 'imag-part '(rectangular) imag-part)
  (put 'magnitude '(rectangular) magnitude)
  (put 'angle '(rectangular) angle)
  (put 'make-from-real-imag 'rectangular
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'rectangular
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

;; ======================================================================
;;
;; The polar number package
;;
;; ======================================================================
(define (install-polar-package)
  ;; internal procedures
  (define (magnitude z) (car z))
  (define (angle z) (cdr z))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z)
    (* (magnitude z) (cos (angle z))))
  (define (imag-part z)
    (* (magnitude z) (sin (angle z))))
  (define (make-from-real-imag x y)
    (cons (sqrt (+ (square x) (square y)))
          (atan y x)))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (put 'real-part '(polar) real-part)
  (put 'imag-part '(polar) imag-part)
  (put 'magnitude '(polar) magnitude)
  (put 'angle '(polar) angle)
  (put 'make-from-real-imag 'polar
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'polar
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)


;; ======================================================================
;;
;; The rational number package
;;
;; ======================================================================
(define (install-rational-package)
  ;; internal procedures
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (make-rat n d) (let ((g (gcd n d)))
                           (cons (/ n g) (/ d g))))
  (define (add-rat x y) (make-rat (+ (* (numer x) (denom y))
                                     (* (numer y) (denom x)))
                                  (* (denom x) (denom y))))
  (define (sub-rat x y) (make-rat (- (* (numer x) (denom y))
                                     (* (numer y) (denom x)))
                                  (* (denom x) (denom y))))
  (define (mul-rat x y) (make-rat (* (numer x) (numer y))
                                  (* (denom x) (denom y))))
  (define (div-rat x y) (make-rat (* (numer x) (denom y))
                                  (* (denom x) (numer y))))
  ;; interface to rest of the system
  (define (tag x) (attach-tag 'rational x))
  (put 'add '(rational rational) (lambda (x y) (tag (add-rat x y))))
  (put 'sub '(rational rational) (lambda (x y) (tag (sub-rat x y))))
  (put 'mul '(rational rational) (lambda (x y) (tag (mul-rat x y))))
  (put 'div '(rational rational) (lambda (x y) (tag (div-rat x y))))
  (put 'make 'rational (lambda (n d) (tag (make-rat n d))))
  'done)

;; ======================================================================
;;
;; Generic procedures
;;
;; ======================================================================

; Constructors
(define (make-scheme-number n)
  ((get 'make 'scheme-number) n))
(define (make-rational numer denom)
  ((get 'make 'rational) numer denom))
(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag 'complex) x y))
(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang 'complex) r a))

(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))
(define (real-part z) (apply-generic 'real-part z))
(define (imag-part z) (apply-generic 'imag-part z))
(define (magnitude z) (apply-generic 'magnitude z))
(define (angle     z) (apply-generic 'angle     z))


;; ======================================================================
;;
;; Package installation
;;
;; ======================================================================
(define (install-number-packages)
  (install-scheme-number-package)
  (install-polar-package)
  (install-rectangular-package)
  (install-rational-package)
  (install-complex-package))

(install-number-packages)


(define z-polar (make-complex-from-mag-ang 5 36.86989764584402129686))
(define z-rect  (make-complex-from-real-imag 3 4))

(magnitude z-rect)
; => 5
(magnitude z-polar)
; => 5


