#lang racket

;; Ex 2.20
;;
(define (same-parity head . tail)
  
  (define (filter-parity equal-parity? others) 
    (if (null? others) 
        null
        (let ((current (car others)))
          (if (equal-parity? current) 
              (cons current (filter-parity equal-parity? (cdr others))) 
              (filter-parity equal-parity? (cdr others))))))
  
  (cons head (filter-parity (if (even? head) 
                                even?
                                odd?)
                            tail)))


