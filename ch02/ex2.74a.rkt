#lang racket

;; Ex 2.74a
;;
(define table (make-hash))
(define (put op type item)
  (hash-set! table (cons op type) item))
(define (get op type)
  (hash-ref table (cons op type) #f)) 

(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum -- TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum -- CONTENTS" datum)))

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (error
           "No method for these types -- APPLY-GENERIC"
           (list op type-tags))))))


; Each division has their own file with its own structure. HQ needs to create its own files that
; contains the division name and the original file from the division. 
(define (make-hq-file division file)
  (cons division file))
(define (file-division hq-file)
  (car hq-file))
(define (original-file hq-file)
  (cdr hq-file))

; Each division must supply two procedures, one to get an emplyees record given their key and 
; one to determine if a given employee key is held in the division's file. 
; These are installed in HQ's operations table.
(define (get-record employee hq-file)
  ((get 'get-record (file-division hq-file))
   employee (original-file hq-file)))

(define (has-record? employee division)
  ((get 'has-record? division) employee))

