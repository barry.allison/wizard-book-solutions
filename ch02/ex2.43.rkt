#lang racket

;; Ex 2.43
;;
;(define (queens board-size)
;  ...
;  ...
;  (define (queen-cols k)  
;    (if (= k 0)
;        (list empty-board)
;        (filter
;         (lambda (positions) (safe? positions))
;         (flatmap
;          (lambda (rest-of-queens)
;            (map (lambda (new-row)
;                   (adjoin-position new-row rest-of-queens))
;                 (queen-cols (- k 1))))
;          (enumerate-interval 1 board-size)))))
;  
;  (queen-cols board-size))

;queen-cols original is called N+1 times (last call is escape clause N=0)
;adjoin position is called N times so run-time is
;N(N+1) : O(N^2)
;
;Loius' queen-cols:
;For each call to queen-cols there are N calls to queen-cols (N-1)
;QN  : N*(QN-1)
;QN-1: N*(QN-2)
;QN-2: N*(QN-3)
;...
;Q3  : N*(Q2)
;Q2  : N*(Q1)
;Q1  : N
;N^N : O(N^N)
;N^(N-2)T
;
