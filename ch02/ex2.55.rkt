#lang racket

;; Ex 2.55
;; 
(car ''abracadabra)
(car (quote (quote abracadabra)))
(car (quote (quote abracadabra)))
(car '(quote abracadabra))
'quote
