#lang racket

;;;  Ex 1.36
;;

(define phi-tolerance 0.00001)
(define (close-enough? v1 v2 tolerance)
  (< (abs (- v1 v2)) tolerance))

(define (print-fixed-point f first-guess)
  (define (try guess)
    (let ((next (f guess)))
      (printf "~a -> ~a~n" guess next)
      (if (close-enough? guess next phi-tolerance)
          next
          (try next))))
  (try first-guess))

(define (average v1 v2)
  (/ (+ v1 v2) 2))

(define (x^x=1000)
  (let* ((tries 0)
         (solution (print-fixed-point (lambda (x) 
                                        (set! tries (add1 tries)) 
                                        (/ (log 1000) (log x)))
                                      3.0)))
    (printf "~ntook ~a tries~n" tries)
    solution))

(define (x^x=1000-damped)
  (let* ((tries 0)
         (solution (print-fixed-point (lambda (x) 
                                        (set! tries (add1 tries)) 
                                        (average x (/ (log 1000) (log x))))
                                      3.0)))
    (printf "~ntook ~a tries~n" tries)
    solution))

;;(x^x=1000-damped)
;;3.0 -> 4.643854911434076
;;4.643854911434076 -> 4.571212264484558
;;4.571212264484558 -> 4.558225323866829
;;4.558225323866829 -> 4.555994244552759
;;4.555994244552759 -> 4.555613793442989
;;4.555613793442989 -> 4.5555490009596555
;;4.5555490009596555 -> 4.5555379689379265
;;4.5555379689379265 -> 4.55553609061889
;;
;;took 8 tries
;;4.55553609061889