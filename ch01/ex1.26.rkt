#lang racket

;;;  Ex 1.26
;;;
(define (louis-expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (* (louis-expmod base (/ exp 2) m)
                       (louis-expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (louis-expmod base (- exp 1) m))
                    m))))

; Louis has approximately doubled the number of calls to louis-expmod because the expression
; (* (louis-expmod base (/ exp 2) m)
;    (louis-expmod base (/ exp 2) m))
; using square evaluates (louis-expmod base (/ exp 2) m)) once and passes the value to square.

