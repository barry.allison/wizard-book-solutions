#lang racket

(define (average a b)
  (/ (+ a b) 2))

(define (cube x) (* x x x))

(define phi-tolerance 0.00001)

(define (close-enough? v1 v2 tolerance)
  (< (abs (- v1 v2)) tolerance))

(define (fixed-point f first-guess)
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next phi-tolerance)
          next
          (try next))))
  (try first-guess))

(define (compose f g)
  (lambda (x)
    (f (g x))))

(define (repeated f n)
  (cond ((= n 1) f)
        (else (compose f (repeated f (sub1 n))))))


;;;  Ex 1.45
;;
(define (sq-root x)
  (fixed-point-of-transform (lambda (y) (/ x y))
                            average-damp
                            1.0))

(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (cube-root x)
  (fixed-point-of-transform (lambda (y) (/ x (sqr y)))
                            average-damp
                            1.0))
;(define (4th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (cube y)))
;                            (repeated average-damp 2)
;                            1.0))
;
;(define (7th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 6)))
;                            (repeated average-damp 2)
;                            1.0))
;
; I HAZ FAIL
;(define (8th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 7)))
;                            (repeated average-damp 2)   ;; use 3
;                            1.0))
;
;(define (15th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 14)))
;                            (repeated average-damp 3)
;                            1.0))
;
; I HAZ FAIL
;(define (16th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 15)))
;                            (repeated average-damp 3)
;                            1.0))

(define (nth-root x n)
  (define (log2 n) (/ (log n) (log 2)))
  (fixed-point-of-transform (lambda (y) (/ x (expt y (sub1 n))))
                            (repeated average-damp (floor (log2 n)))
                            1.0))

