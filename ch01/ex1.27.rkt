#lang racket

;;;  Ex 1.27
;;;
(define (divides? a b) (= (remainder b a) 0))
(define (smallest-divisor n)
  (define (find-divisor n test-divisor)
    (cond ((> (sqr test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (+ test-divisor 1)))))
  (find-divisor n 2))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (sqr (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (big-random n)
  (inexact->exact (floor ( * (random) n))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (big-random (- n 1)))))

(define (carmichael? n)
  (define (fermat-iter n a)
    (cond ((= a 1) true)
          ((fermat-test n) (fermat-iter n (- a 1)))
          (else false)))
  (fermat-iter n (- n 1)))
  

(define carmichaels '(561 1105 1729 2465 2821 6601))

(map carmichael? carmichaels)
(map fermat-test carmichaels)
(map prime? carmichaels)
